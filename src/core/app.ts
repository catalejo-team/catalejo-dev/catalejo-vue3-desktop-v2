// Adds js/ts components
import type { Project, FilePath, Result, Plugin, PluginDictionary } from "./types";
import * as request from "./request"
import { EXTENSIONS, EXT_BLOCKLY, EXT_CODEMIRROR, STATUS, WORKSPACE, SO } from "./consts"
import { identifyPathAndSO } from "@/core/general";

// -- INFO: START PROJECTS --
/**
  * Esta función verifica si un path está dentro de projects
  * @param {Project[]} projects - Referencia de los proyectos cargados
  * @param {string} path - Path del archivo para buscar entre los proyectos a cargar
  * @returns {Result} {} - Solo el estado es necesario.
  * */
function projectExists(projects: Project[], path: string): Result {
  for (let index = 0; index < projects.length; index++) {
    if (projects[index]["path"] === path) {
      return { status: STATUS.EXISTS, data: index };
    }
  }
  return { status: STATUS.NOEXIST };
}

/**
  * Función modificadora de projects: en vista que projects es un array,
  * cualquier modificación a projects se manifesta en la variable global
  * de projects, por tanto se debe crear la variable projects: Project = [];
  * @param {Project[]} projects - Referencia de los proyectos cargados
 */
export async function addProject(projects: Project[], path: string): Promise<Result> {
  // Obtener las características del archivo
  const filepath = getFilenameAndExt(path)
  // El archivo a carga tiene extension valida?
  if (filepath.extension == "") {
    return { status: STATUS.WITHOUTCHANGE };
  }
  // La extension no es soportada por el editor?
  if (!EXTENSIONS.includes(filepath.extension)) {
    return { status: STATUS.WITHOUTCHANGE };
  }
  // El archivo ya esta cargado como proyecto?
  const res = projectExists(projects, filepath.path)
  if (res.status == STATUS.EXISTS) {
    return { status: STATUS.REQUEST_SELECT_FILE_IN_TABVIEW, data: res.data }
  } else {
    // Solicitar contenido del archivo
    const result = await request.readFile(filepath.path);
    if (result.status == 0) {
      // si el archivo es un json obtener contenido
      const jsondata = (filepath.extension == "json") ? JSON.parse(result.data) : {};
      const config = getConfig(result.data, filepath.extension);
      // Cargar archivo como proyecto del editor
      // const jsondata = {};
      const project: Project = {
        path: filepath.path,
        filename: filepath.filename,
        extension: filepath.extension,
        content: result.data,
        blocklycode: jsondata.blocklycode || undefined,
        codemirrorCursor: 0,
        config: config,
        status: STATUS.ADDED,
      };
      projects.push(project)
      return {
        status: STATUS.REQUEST_SELECT_FILE_IN_TABVIEW,
        data: (projects.length - 1)
      }
    } else {
      return { status: STATUS.WITHOUTCHANGE };
    }
  }
}

// TODO: Es posible que integrar las funciones de addProject y newProject en uno solo
/** Permite crear un nuevo proyecto a través del path y del contenido */
export async function newProject(projects: Project[], path: string, content: string): Promise<Result> {
  // Obtener las características del archivo
  const filepath = getFilenameAndExt(path)
  // El archivo a carga tiene extension valida?
  if (filepath.extension == "") {
    return { status: STATUS.WITHOUTCHANGE };
  }
  // La extension no es soportada por el editor?
  if (!EXTENSIONS.includes(filepath.extension)) {
    return { status: STATUS.WITHOUTCHANGE };
  }
  // El archivo ya esta cargado como proyecto?
  const res = projectExists(projects, filepath.path)
  if (res.status == STATUS.EXISTS) {
    return { status: STATUS.REQUEST_SELECT_FILE_IN_TABVIEW, data: res.data }
  } else {
    // Solicitar contenido del archivo
    if (content != "") {
      // si el archivo es un json obtener contenido
      const jsondata = (filepath.extension == "json") ? JSON.parse(content) : {};
      const config = getConfig(content, filepath.extension);
      // Cargar archivo como proyecto del editor
      // const jsondata = {};
      const project: Project = {
        path: filepath.path,
        filename: filepath.filename,
        extension: filepath.extension,
        content: content,
        blocklycode: jsondata.blocklycode || undefined,
        codemirrorCursor: 0,
        config: config,
        status: STATUS.ADDED,
      };
      projects.push(project)
      return {
        status: STATUS.REQUEST_SELECT_FILE_IN_TABVIEW,
        data: (projects.length - 1)
      }
    } else {
      return { status: STATUS.WITHOUTCHANGE };
    }
  }
}

/** Remover un project del array project haciendo uso del indice */
export function removeProject(projects: Project[], index: number): Result {
  if (projects !== undefined && projects.length > index) {
    projects.splice(index, 1);
    return { status: STATUS.OK };
  }
  return { status: STATUS.FAILED };
}

// Actualizar proyecto desde codemirror
export function updateProjectFromCodemirror(
  projects: Project[],
  content: string,
  codemirrorCursor: number,
  index: number
): Result {
  projects[index].content = content;
  projects[index].codemirrorCursor = codemirrorCursor;
  projects[index].status = STATUS.UNSAVED
  return { status: STATUS.OK };
}

// Actualizar proyecto desde blockly
export function updateProjectFromBlockly(
  projects: Project[],
  blocklycode: string,
  index: number
): Result {
  projects[index].blocklycode = blocklycode;
  projects[index].status = STATUS.UNSAVED
  return { status: STATUS.OK };
}
// -- END PROJECTS --

// Según el proyecto a cargar se selecciona el
// entorno del workspace
export function setWorkspace(project: Project): Result {
  if (EXT_BLOCKLY.includes(project.extension)) {
    return { status: STATUS.SET_WS, data: WORKSPACE.BLOCKLY };
  } else if (EXT_CODEMIRROR.includes(project.extension)) {
    return { status: STATUS.SET_WS, data: WORKSPACE.CODEMIRROR };
  } else {
    return { status: STATUS.SET_WS, data: WORKSPACE.WELCOME };
  }
}

// -- ACTUALIZACIONES DE CONFIGURACIÓN Y CONTENIDO DEL PROYECTO --
/** Actualizar el contenido de un proyecto.
 * Cuando se trata de un proyecto que está en formato plano,
 * simplemente se obtiene el contenido de forma directa,
 * sin embargo, si es de formato json y además es de tipo blockly,
 * debe compilarse el contenido y actualizar el json asociado a 
 * el formato de blockly.
 */
function updateContentOfProject(projects: Project[], index: number): Result {
  const project = projects[index];
  if (project.extension == "json" && project.blocklycode !== undefined) {
    const content = {
      config: project.config || "", // Pueda que no existan opciones asociadas
      blocklycode: project.blocklycode, // esto indica que es un archivo que se carga con blockly
    }
    projects[index].content = JSON.stringify(content);
  }
  return { status: STATUS.OK }
}

/** Actualizar la información de configuraciób de un proyecto
 * a través del contenido del archivo (en el caso de los archivo
 * de texto) o desde una interfaz para un archivo json */
function updateConfigOfProject(projects: Project[], index: number, blocklyConfig?: any): Result {
  const project = projects[index];
  let config: any;
  // La extensión es soportada por blockly y aedmás existe una solicitud externa de configuración?
  if (EXT_BLOCKLY.includes(project.extension) && blocklyConfig != undefined) {
    // requiere una configuración externa
    config = blocklyConfig;
    // la extensión es soportada por codemirror
  } else if (EXT_CODEMIRROR.includes(project.extension)) {
    // Obtener desde el contenido la configuración
    config = getConfig(project.content, project.extension)
  } else {
    // No fue posible realzar la configuración
    return { status: STATUS.FAILED };
  }
  // Actualixando la configuración
  projects[index].config = config;
  // Reportando la actualización de la configuración
  return { status: STATUS.OK };
}

// -- INFO: PARSES --
/**
 * selector de parseo para obtener la configuración
 */
function getConfig(data: string, extension: string) {
  if (extension == "lua") {
    return parseLuaConfig(data);
  } else if (extension == "py") {
    return parsePythonConfig(data);
  } else if (extension == "json") {
    return parseJsonConfig(data);
  } else {
    return {};
  }
}

/**
 * Obtener configuraciones desde comentarios de un script de python
 */
function parsePythonConfig(content: string) {
  const config: any = {};
  const lines = content.split("\n");
  for (const line of lines) {
    const match = line.match(/# (\w+): (.+)/);
    if (match) {
      const [, key, value] = match;
      config[key] = String(value);
    }
  }
  return config;
}

/**
 * Obtener configuraciones desde comentarios de un script de lua
 */
function parseLuaConfig(content: string) {
  const config: any = {};
  const lines = content.split("\n");
  for (const line of lines) {
    const match = line.match(/-- (\w+): (.+)/);
    if (match) {
      const [, key, value] = match;
      config[key] = String(value);
    }
  }
  return config;
}

/**
 * Obtener configuraciones desde un json
 */
function parseJsonConfig(content: string) {
  const config = JSON.parse(content).config;
  if (config === undefined) {
    return {};
  }
  return config;
}

//-- CONFIG PROYECTS --
/** Formatear la configuración de un proyecto
 * para el dialogo de configProjectForDialog */
export function getConfigProjectForDialog(project: Project) {
  let items: any[] = [];
  if (project.config != undefined) {
    Object.keys(project.config).forEach(key => {
      items.push({
        label: [key],
        value: project.config[key]
      })
    });
    items.push({
      label: "",
      value: ""
    })
  }
  return items;
}

/** Tomar la configuración formateada en item dialogo y
 * devolverla como un objeto de configuración de un proyecto */
export function setConfigProjectFromItemsDialog(items: { label: string, value: string }[]) {
  let config: { [key: string]: string } = {};
  for (let index = 0; index < items.length; index++) {
    const key = items[index].label;
    if (key != "" && key != undefined) {
      const value = items[index].value;
      config[key] = value;
    }
  }
  return config;
}

//-- INFO: START PATH FILE AND DIR --

/** Obtener el path de un archivo desde un string */
function getFilenameAndExt(path: string): FilePath {
  const pathFeatures = identifyPathAndSO(path);
  if (pathFeatures.so == SO.UNIX) {
    const filename = path.split('/').pop();
    const extension = path.includes('.') ?
      path.split('.').pop() : "";
    return {
      path: path,
      filename: filename ? filename : "",
      extension: extension ? extension : "",
    };
  } else if (pathFeatures.so == SO.WINDOWS) {
    const filename = path.split('\\').pop();
    const extension = path.includes('.') ?
      path.split('.').pop() : "";
    return {
      path: path,
      filename: filename ? filename : "",
      extension: extension ? extension : "",
    };
  } else {
    // TODO: Revisar este else, no ha sido comprobado
    return {
      path: "",
      filename: "",
      extension: ""
    }
  }
}

/** Solicitud de cargar contenido de un directorio */
export async function getDirContent(dirPath: string): Promise<Result> {
  return await request.getDirContent(dirPath);
}

// -- SAVE, READ, DELETED PROJECTS
/**
 * Guardar cambios en el archivo y repotar si fue posible o no
 */
export async function saveProject(
  projects: Project[], index: number
): Promise<Result> {
  // Actualizar el contenido a guardar en el file
  updateContentOfProject(projects, index);
  // Actualizar las configuraciones del proyecto
  updateConfigOfProject(projects, index);
  // realizar el request para guardar
  const res = await request.saveFile(projects[index].path, projects[index].content);
  return res
}

export async function deleteFile(path: string): Promise<Result> {
  const res = await request.deleteFile(path);
  return res;
}

export async function uploadFile(file: FormData): Promise<Result> {
  const res = await request.uploadFile(file);
  return res;
}

//-- PLUGINS ADMINISTRATIONS --
/** Desde el array plugin[] agregar a plugins dictionary
 * para que a través del nombre de la plataforma se pueda usar específicamente la función 
 * del plugin y asociarlo al contenido del proyecto (archivo). */
export function addPlugins(plugins: PluginDictionary, plugin: Plugin[]) {
  // Hay plugins a agregar?
  if (plugin.length > 0) {
    // Recorrer el arreglo de plugin y asignar sus valores al objeto plugin, es decir, al diccionario
    for (let index = 0; index < plugin.length; index++) {
      plugins[plugin[index].platform] = plugin[index];
    }
  }
}
