import { type Ref } from "vue";
import * as Blockly from "@johnnycubides/blockly-catalejo/";
import * as Es from "@johnnycubides/blockly-catalejo/msg/es";
import * as En from "@johnnycubides/blockly-catalejo/msg/en";
import { LANG, STATUS, LUA, PYTHON, CHUCK, JSON, JAVASCRIPT } from "./consts";
import type { Plugin, Result } from "./types";

export const toolbox = `

<xml id="toolbox" style="display: none">
  <category name="Lógica" colour="%{BKY_LOGIC_HUE}">
    <block type="controls_if"></block>
  </category>
  <category name="Secuencias" colour="%{BKY_LOOPS_HUE}">
    <block type="controls_repeat_ext">
      <value name="TIMES">
        <shadow type="math_number">
          <field name="NUM">10</field>
        </shadow>
      </value>
    </block>
  </category>
</xml>`;
// <xml>
//   <block type="controls_if"></block>
//   <block type="controls_whileUntil"></block>
// </xml>`;

// TODO: se requiere hacer una modificación que permita que la orientación sea cargada de manera dinámica
function setHorizontalLayout() {
  const windowWidth = window.innerWidth;
  const windowHeight = window.innerHeight;
  // Si el ancho < alto
  if (windowWidth < windowHeight) {
    return true;
  } else {
    return false;
  }
}

const options = {
  media: "media/",
  // theme: theme, // DarkTheme funciona con blockly 9.0 en adelante
  renderer: "geras", // geras, thrasos, zelos
  toolboxPosition: "start", // [toolboxPosition, horizontalLayout] top: [start, true], bottom:end-true, left: [start, false], right:[end, false]
  // toolboxPosition: Blockly.utils.toolbox.Position.TOP, // [toolboxPosition, horizontalLayout] top: [start, true], bottom:end-true, left: [start, false], right:[end, false]
  horizontalLayout: setHorizontalLayout(),
  // scrollbars: true,
  // toolboxHorizontalScroll: true,
  zoom: {
    controls: true,
    wheel: true,
    startScale: 1.0,
    maxScale: 3,
    minScale: 0.3,
    scaleSpeed: 1.2,
    pinch: true,
  },
  trashcan: true,
  grid: {
    spacing: 25,
    length: 1,
    colour: "#ccc",
    snap: true,
  },
  toolbox: toolbox,
};

export const code =
  '<xml xmlns="https://developers.google.com/blockly/xml"></xml>';

// Inyectar blockly en el div correspodiente y dejar la referencia
// en la variable de workspace
export function initWorkspaceBlockly(
  blocklyDiv: Ref,
  // props: any
) {
  return Blockly.inject(blocklyDiv.value, options);
  // const code2xml = Blockly.utils.xml.textToDom(code);
  // Blockly.Xml.clearWorkspaceAndLoadFromXml(code2xml, workspace);
}

/**
 * Función para inyectar bloques en el workspace
 * @param {string} blocks representación de bloques en string xml
 * @param workspace el lugar donde se van a inyectar los bloques
 * @returns {Result} Result indica si falló o no en el proceso de inyectar
 */
export function injectBlocksToWorkspace(blocks: string, workspace: Blockly.WorkspaceSvg): Result {
  const code2xml = Blockly.utils.xml.textToDom(blocks);
  try {
    Blockly.Xml.clearWorkspaceAndLoadFromXml(code2xml, workspace);
    return { status: STATUS.OK }
  } catch (error) {
    return { status: STATUS.FAILED }
  }
}

// Seleccionar el lenguaje de los elementos de blockly
export function setLanguage(lang: LANG) {
  if (lang == LANG.ES) {
    Blockly.setLocale(Es)
  } else {
    Blockly.setLocale(Es)
  }
}

// TODO: Esta función requiere una revisión que permita posicionar el layout de manera dinámica.
export function setPositionLayout(workspace: Blockly.WorkspaceSvg) {
  const windowWidth = window.innerWidth;
  const windowHeight = window.innerHeight;
  // Si el ancho < alto y horizontal no está activo
  if (windowWidth < windowHeight) {
    workspace.options.horizontalLayout = true;
  } else {
    workspace.options.horizontalLayout = false;
  }
  Blockly.svgResize(workspace);
}

//-- START IMPORT GENERATOR --
import { luaGenerator } from "@johnnycubides/blockly-catalejo/lua";
import { pythonGenerator } from "@johnnycubides/blockly-catalejo/python";
import { stringSearch } from "./general";

/** Seleccionar un generador en función del lenguaje del plugin */
function getGenerator(plugin: Plugin) {
  switch (plugin.platformLang.name) {
    case LUA.name:
      return luaGenerator;
    case PYTHON.name:
      return pythonGenerator;
    default:
      return;
  }
}
//-- END IMPORT GENERATOR --

//-- START LOAD PLUGIN --
/** Guarda una lista de los plugins que han sido cargados por blockly 
 * a través del nombre de la plataform */
let pluginLoaded: string[] = [];

/** Iniciar un plugin el cual requiere la referencia del
 * 1. workspace, 2. tipo de generador, 3. referencia a Blockly
 */
export function initPluginBlockly(
  plugin: Plugin,
  workspace: Blockly.WorkspaceSvg,
) {
  // El plugin de blockly no ha sido cargado?
  if (stringSearch(pluginLoaded, plugin.platform).status == STATUS.NOTFOUND) {
    // Habilitar blockly
    plugin.initBlockly(
      Blockly,
      getGenerator(plugin),
      workspace
    );
    // TODO: debe haber un return que habilite ese enable
    // Inidica que blockly fue habilitado
    pluginLoaded.push(plugin.platform);
  }
}
//-- END LOAD PLUGIN --
