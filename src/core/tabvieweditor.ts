import type { Project } from './types'
import { STATUS } from './consts';

export function putItemIntabView(projects: Project[]): { title: string, indexOfProject: Number }[] {
  let itemsTabView = [];
  for (let index = 0; index < projects.length; index++) {
    itemsTabView.push({
      title: projects[index].filename,
      indexOfProject: index
    })
  }
  return itemsTabView;
}

/**
 * @function setTitleInTabview
 * Actualiza un tabview en función si el proyecto
 * tiene cambios que no se han guardado.
 */
export function setTitleInTabview(projects: Project[], index: number) {
  const unsave = (projects[index].status == STATUS.UNSAVED) ? "*" : "";
  return {
    title: projects[index].filename + unsave,
    indexOfProject: index
  }
}
