import type { STATUS, TYPE_DIALOG_REQUEST, TYPE_PLUGIN_RESULT, TYPE_TERMINAL_CONNECTION } from "./consts";

/** Lenguajes de programación */
export interface ProgLang {
  name: string,
  extension: string,
}

/** Estructura de los proyectos a guardar */
export interface Project {
  path: string,
  filename: string,
  extension: string, // la extensión está asociada al lenguaje de codemirror
  content: string,  // Contenido a guardar en el File
  blocklycode: string, // compilación dada por ejemplo desde blockly XML
  codemirrorCursor: number // Posición del cursor
  config: any,
  status: STATUS, // ADDED, SAVED, UNSAVED
}

// Información obtenida para los archivos
// a cargar en el treeview y que será entregada
// a la app en el caso de solicitarse un archivo
// a cargar en projects[]
export interface FilePath {
  path: string,
  extension: string,
  filename: string,
}

export interface DirTreeView {
  label: string,
  path: string
}

// Formato de retorno para funciones
// especialmente como interfaz de vue
export interface Result {
  status: STATUS
  data?: any
}

/** Respuesta que debe entregar el plugin para usar en el editor */
export interface PluginResult {
  typeResult: TYPE_PLUGIN_RESULT
  data?: any
}

export interface DataToast {
  severity: string, /** "error" | "success" | "secondary" | "info" | "contrast" | "warn" */
  summary: string,
  detail: string,
  life: number, /** time in ms */
}

export interface DataDialog {
  title: string,
  content: any,
  emit: string,
  args: any,
  description?: string,
  okLabel?: string,
  cancelLabel?: string,
}

/** Dialogo solicitado por funciones genéricas del própio editor */
export interface DialogRequest {
  typeDialog: TYPE_DIALOG_REQUEST,
  data: DataDialog
}

/** Datos requeridos para websocket **/
export interface WebsocketData {
  ip: string, // ip del websocket server
  port: string,  // número del puerto websocket a conectarse
  user: string, // Nombre de usuario para login
  password: string,  // Password del usuario para login
  endOfCommand: string, // Caracteres a agregar al final del comando a enviar
}

/** Typo de datos para una conexión por terminal */
export interface RequestTerminalConnection {
  typeConnection: TYPE_TERMINAL_CONNECTION,
  data?: WebsocketData,
}

// Formato de los archivos presentados
// en el treeview
export interface ItemsTreeView {
  key: string,
  label: string,
  leaf: boolean,
  icon: string,
  data: string,
  extension: string,
}

export interface DataWorkspace {
  indexOfProjects: number, // Index del proyecto a actualizar
  content: string,  // Contenido a guardar
  xmlcode?: string, // Si es blockly se entrega la compilación según lenguaje
}

// TODO: cambiar lang al nombre en vez de usar la extensión.
export interface Plugin {
  [platform: string]: any,
  platformLang: ProgLang, // Lenguaje principal (usar la extensión)
  toolbox: string,
  initBlockly: Function,
  generateCode: Function,
  getItemsToolBarOfPlugin: Function,
  commandEmit: Function,
  blockly_template: string,
  lang_template: string,
}

export interface PluginDictionary {
  [key: string]: Plugin;
};

