import { python } from '@codemirror/lang-python';
import { StreamLanguage } from "@codemirror/language";
import { lua } from "@codemirror/legacy-modes/mode/lua";
import { LUA, PYTHON } from "./consts";

export function setLang(extension: string) {
  switch (extension) {
    case PYTHON.extension:
      return python();
    case LUA.extension:
      return StreamLanguage.define(lua);
    default:
      return;
  }
}
