import { EXTENSIONS, STATUS, SO } from "./consts";
import type { DirTreeView, FilePath, ItemsTreeView, Result } from "./types";
import type { TreeNode } from "primevue/treenode";
import { identifyPathAndSO } from "@/core/general";

// WARNING: Duplicado en app.ts, quitar.
function getFilenameAndExt(path: string): FilePath {
  const pathFeatures = identifyPathAndSO(path);
  if (pathFeatures.so == SO.UNIX) {
    const filename = path.split('/').pop();
    const extension = path.includes('.') ?
      path.split('.').pop() : "";
    return {
      path: path,
      filename: filename ? filename : "",
      extension: extension ? extension : "",
    };
  } else if (pathFeatures.so == SO.WINDOWS) {
    const filename = path.split('\\').pop();
    const extension = path.includes('.') ?
      path.split('.').pop() : "";
    return {
      path: path,
      filename: filename ? filename : "",
      extension: extension ? extension : "",
    };
  } else {
    // TODO: Revisar este else, no ha sido comprobado
    return {
      path: "",
      filename: "",
      extension: ""
    }
  }
}

function setIcon(ext: string) {
  return EXTENSIONS.includes(ext) ? "pi pi-file-edit" : ""
}

// INFO: Función para obtener los items a agregar en el treeview
// a partir del directorio de trabajo
export function getFileItems(pathArray: string[]): TreeNode[] {
  let items = []
  for (let i = 0; i < pathArray.length; i++) {
    const element = getFilenameAndExt(pathArray[i]);
    items.push({
      key: String(i),
      label: element.filename,
      leaf: true,
      icon: element.extension != "" ? setIcon(element.extension) : "",
      data: element.path,
      extension: element.extension
    })
  }
  return items
}

// console.log(getFilenameAndExt("/home/johnny/projects/catalejo-lua/work/hello.py"));
// console.log(getFilenameAndExt("C:\\Users\\Johnny\\Documents\\archivo.txt"));

// -- PATH OF DIRECTORIES
// Se plantea el manejo de directorios que pueden ser cargados en el editor,
// así el usuario puede tener diferentes directorios de referencia
/** Obtener el path y el nombre de un direcotio */
function getDirnameAndPath(path: string): Result {
  const pathFeatures = identifyPathAndSO(path);
  if (pathFeatures.so == SO.UNIX) {
    const filename = path.split('/').pop();
    const extension = path.includes('.') ?
      path.split('.').pop() : "";
    if (extension != "") {
      return { status: STATUS.FAILED }
    }
    return {
      status: STATUS.OK, data: {
        path: path,
        dirname: filename ? filename : "noname",
      }
    };
  } else if (pathFeatures.so == SO.WINDOWS) {
    const filename = path.split('\\').pop();
    const extension = path.includes('.') ?
      path.split('.').pop() : "";
    if (extension != "") {
      return { status: STATUS.FAILED }
    }
    return {
      status: STATUS.OK, data: {
        path: path,
        dirname: filename ? filename : "noname",
      }
    };
  } else {
    return {
      status: STATUS.FAILED
    }
  }
}
/** Crear items para el treeview sobre los directorios */
export function getDirItems(paths: string[]): TreeNode[] {
  let items = []
  for (let i = 0; i < paths.length; i++) {
    const element = getDirnameAndPath(paths[i]);
    if (element.status == STATUS.OK) {
      items.push({
        key: String(i),
        label: element.data.dirname,
        leaf: true,
        icon: 'pi pi-folder',
        data: element.data.path,
      })
    }
  }
  return items
}

