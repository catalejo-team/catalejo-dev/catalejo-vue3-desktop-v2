/** Los request son el médio por el cual se interactúa con el backend */

import { STATUS } from "./consts";
import type { Result } from "./types";

// const HOST = "http://192.168.2.103";
// const HOST = "http://localhost";
const HOST = "http://" + location.hostname
const PORT = "8080";
const MODE = "cors" // cors, no-cors, same-origin

async function fetchRequest(url: string, options: any): Promise<Result> {
  try {
    const response = await fetch(url, options);
    const result = await response.json();
    // TODO: Quitar o modificar, se ha usado para debug.
    if (result.status == STATUS.OK) {
      return result;
    } else {
      return result;
    }
  } catch (error) {
    console.error(error);
    return { status: STATUS.FAILED };
  }
}

// TODO: Confirmar si la siguiente función está siendo usada. En caso contrario borrar.
export async function dirconfig(path: string) {
  const ROUTE = "fs/dirconfig"
  const url = HOST + ":" + PORT + '/' + ROUTE;
  const options: RequestInit = {
    method: 'PUT',
    mode: MODE,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ workdir: path })
  };

  try {
    const response = await fetch(url, options);
    const result = await response.json();
    // console.log(result);
    return result;
  } catch (error) {
    console.error(error);
    return { result: -1 };
  }
}

export async function getDirContent(path: string): Promise<Result> {
  // TODO: cambiar el ROUTE que councida con el getDirContent
  const ROUTE = "fs/dirconfig"
  const url = HOST + ":" + PORT + '/' + ROUTE;
  const options: RequestInit = {
    method: 'PUT',
    mode: MODE,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ workdir: path })
  };
  try {
    const response = await fetch(url, options);
    const result = await response.json();
    if (result.status == STATUS.OK) {
      return result;
    } else {
      return { status: STATUS.FAILED };
    }
  } catch (error) {
    console.error(error);
    return { status: STATUS.FAILED };
  }
}

/**
 * Retorna el contenido
 * a través del path
 * @returns status, data
 */
export async function readFile(path: string) {
  const ROUTE = "fs/readFile"
  const url = HOST + ":" + PORT + '/' + ROUTE;
  const options: RequestInit = {
    method: 'PUT',
    mode: MODE,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ file: path })
  };

  try {
    const response = await fetch(url, options);
    const result = await response.json();
    // console.log(data);
    if (result.status == STATUS.OK) {
      return result;
    }
  } catch (error) {
    console.error(error);
    return { status: -1 };
  }
}

/** Guardar contenido en un archivo específico */
export async function saveFile(path: string, content: string): Promise<Result> {
  const ROUTE = "fs/writeFile"
  const url = HOST + ":" + PORT + '/' + ROUTE;
  const options: RequestInit = {
    method: 'PUT',
    mode: MODE,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ file: path, content: content })
  };

  try {
    const response = await fetch(url, options);
    const result = await response.json();
    // console.log(data);
    if (result.status == STATUS.OK) {
      return result;
    } else {
      return { status: STATUS.FAILED };
    }
  } catch (error) {
    console.error(error);
    return { status: STATUS.FAILED };
  }

}


export async function listFiles() {
  const ROUTE = "fs/listFiles";
  const url = HOST + ":" + PORT + '/' + ROUTE;
  const options: RequestInit = {
    method: 'GET',
    mode: MODE,
    headers: { 'Access-Control-Allow-Origin': HOST },
    body: undefined,
  };
  try {
    const response = await fetch(url, options);
    const data = await response.json();
    console.log(data);
    return data;
  } catch (error) {
    console.error(error);
    return error;
  }
}

export async function deleteFile(path: string): Promise<Result> {
  const ROUTE = "fs/deleteFile"
  const url = HOST + ":" + PORT + '/' + ROUTE;
  const options: RequestInit = {
    method: 'PUT',
    mode: MODE,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ path: path })
  };
  return await fetchRequest(url, options);
}

export async function uploadFile(file: FormData): Promise<Result> {
  const ROUTE = "fs/uploadFile"
  const url = HOST + ":" + PORT + '/' + ROUTE;
  const options: RequestInit = {
    method: 'POST',
    mode: MODE,
    headers: {
      // no requiere indicar el conten-type en fista que es un formato resuelto por el usuario
      // 'Content-Type': 'application/json',
    },
    body: file
  };
  return await fetchRequest(url, options);
}
