import * as request from "./request"

export async function dirconfig(emit: any, path: string) {
  const result = await request.dirconfig(path);
  if (result.status == 0) {
    emit('dirconfig-update', result.data);
  }
}
