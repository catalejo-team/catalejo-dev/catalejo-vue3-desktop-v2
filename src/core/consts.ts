import type { ProgLang } from "./types";
// extensiones que soporta el editor
export const EXTENSIONS = "py lua json ck";

export const EXT_CODEMIRROR = "py lua ck"
export const EXT_BLOCKLY = "json"

/** Nombre del lenguaje del editor de texto en función de la extensión */
export const EXT_NAME: Record<string, string> = {
  "py": "Python",
  "lua": "Lua",
  "ck": "Chuck",
  "json": "JSON",
  "txt": "Text plain"
}

export const PYTHON: ProgLang = { name: "Python", extension: "py" };
export const CHUCK: ProgLang = { name: "ChucK", extension: "ck" };
export const LUA: ProgLang = { name: "Lua", extension: "lua" };
export const JAVASCRIPT: ProgLang = { name: "JavaScript", extension: "js" };
export const JSON: ProgLang = { name: "JSON", extension: "json" };

export const PROG_LANG: ProgLang[] = [
  PYTHON,
  CHUCK,
  LUA,
  JAVASCRIPT,
  JSON,
]


export enum STATUS {
  OK,
  FAILED,
  CHANGE,
  WITHOUTCHANGE,
  REQUEST, // Solicitud para realizar algo
  REQUEST_SELECT_FILE_IN_TABVIEW,
  EXISTS,
  NOEXIST,
  FOUND,
  NOTFOUND,
  ADDED,
  SAVED,
  UNSAVED,
  SET_WS,
  ENABLED,
  DESABLED,
}

/** Tipo de resultados posibles a entregar al editor 8 */
export enum TYPE_PLUGIN_RESULT {
  TOAST, TEXT_PRINT, TEXT_AREA, URL_PRINT, TERM
}

/** Tipo de diálogo solicitado */
export enum TYPE_DIALOG_REQUEST {
  YES_OR_NO_QUESTION, UPLOADFILE,
}

export enum WORKSPACE {
  WELCOME,
  CODEMIRROR,
  BLOCKLY,
  FREE,
}

/** Idioma del editor */
export enum LANG {
  EN,
  ES,
}

/** Sistema operativo */
export enum SO {
  UNIX,
  WINDOWS,
  LINUX,
  MACOS,
  UNKNOW,
}

/** Tipo de conexión para terminal */
export enum TYPE_TERMINAL_CONNECTION {
  WEBSOCKET,
  TCP_IP,
  SERIAL,
}

export enum PATH_TYPE {
  RELATIVE,
  ABSOLUTE,
  UNKNOW,
}
