import { STATUS, SO, PATH_TYPE } from "@/core/consts";
import type { Result } from "@/core/types";

/** Detectar sistema operativo desde el navegador */
export function getSystemOS(): SO {
  const userAgent = navigator.userAgent || navigator.vendor || (window as any).opera;
  if (/windows/i.test(userAgent)) {
    return SO.WINDOWS;
  } else if (/macintosh|mac os x/i.test(userAgent)) {
    return SO.UNIX;
  } else if (/linux/i.test(userAgent)) {
    return SO.UNIX;
  } else {
    return SO.UNKNOW;
  }
}

/** Buscar la exitencia del string dentro del array y reportar la posición */
export function stringSearch(array: string[], str: string): Result {
  for (let index = 0; index < array.length; index++) {
    if (array[index] == str) {
      return { status: STATUS.FOUND, data: index };
    }
  }
  return { status: STATUS.NOTFOUND };
}

/** Identificar la posible proveniencia de un path, saber si este es relativo o absoluto */
export function identifyPathAndSO(path: string): { so: SO, pathType: PATH_TYPE } {
  // Patrones absolutos
  const windowsAbsolutePattern = /^[a-zA-Z]:[\\/]/; // Ej. C:\folder\file.txt
  const unixAbsolutePattern = /^\//; // Ej. /home/user/file.txt
  // Patrones relativos
  const windowsRelativePattern = /^(?:\.\\|\.\.\\|[^\/\\:*?"<>|]+(?:\\|$))/; // Ej. folder\file.txt o .\file.txt
  const unixRelativePattern = /^(?:\.\.\/|\.\/|[^\/]+(?:\/|$))/; // Ej. folder/file.txt o ./file.txt

  // Caso absoluto Windows
  if (windowsAbsolutePattern.test(path)) {
    return { so: SO.WINDOWS, pathType: PATH_TYPE.ABSOLUTE };
  }
  // Caso absoluto Unix
  if (unixAbsolutePattern.test(path)) {
    return { so: SO.UNIX, pathType: PATH_TYPE.ABSOLUTE };
  }
  // Caso relativo Windows
  if (windowsRelativePattern.test(path) && path.includes('\\')) {
    return { so: SO.WINDOWS, pathType: PATH_TYPE.RELATIVE };
  }
  // Caso relativo Unix
  if (unixRelativePattern.test(path) && path.includes('/')) {
    return { so: SO.UNIX, pathType: PATH_TYPE.RELATIVE };
  }
  // 🚨 **Caso ambiguo explícito: "work" u otros nombres sin barras**
  if (/^[^\/\\:*?"<>|]+$/.test(path)) {
    const detectedOS = getSystemOS();
    return { so: detectedOS, pathType: PATH_TYPE.RELATIVE };
  }
  // Caso desconocido
  return { so: SO.UNKNOW, pathType: PATH_TYPE.UNKNOW };
}
