import { LANG } from "./consts"
import { deleteFile } from "./request"

export const treeviewLang = {
  dirTitle: {
    en: "Directories:",
    es: "Directorios:"
  },
  fileTitle: {
    en: "Files:",
    es: "Archivos:"
  }
}

export const toolbarLang = {
  menu: {
    en: "",
    es: ""
  },
  save: {
    en: "Save",
    es: "Guardar"
  },
  close: {
    en: "Close",
    es: "cerrar"
  },
  config: {
    en: "Config",
    es: "Configurar"
  },
}

export const menubarcommonLang = {
  save: {
    en: "Save",
    es: "Guardar"
  },
  close: {
    en: "Close",
    es: "cerrar"
  },
  file: {
    en: "File",
    es: "Archivo"
  },
  opendir: {
    en: "Open dir",
    es: "Abrir directorio"
  },
  selectDirPath: {
    header: {
      en: "Put dir path.",
      es: "Poner ruta de directorio"
    }
  },
  newProject: {
    en: "New project",
    es: "Nuevo proyecto"
  },
  delete: {
    en: "Delete file",
    es: "Borrar archivo"
  },
  fileUpload: {
    en: "Upload files",
    es: "Subir archivos"
  },
  dialogDeleteFile: {
    title: {
      en: "Delete file?",
      es: "¿Borrar archivo?"
    },
    content: {
      en: "Do you want delete the file: ",
      es: "Quiere borrar el archivo: "
    }
  },
  dialogNewProject: {
    header: {
      en: "New project",
      es: "Nuevo proyecto"
    },
    labelInput: {
      en: "1. Insert a file name for project, the extension file will created atomatically.",
      es: "1. Agregar un nombre de archivo para el proyecto, la extensión será creada automáticamente."
    },
    labelDirPath: {
      en: "The project file will created in the path: ",
      es: "El proyecto será creado en el path: "
    },
    labelSelectPlatform: {
      en: "2. Select a platform compatible with editor: ",
      es: "2. Seleccionar una plataforma compatible con el editor: "
    },
    labelSelectProjectType: {
      en: "3. Select project type to create.",
      es: "3. Selecciona el tipo de proyecto a crear."
    }
  },
  dialogButtonYesNo: {
    ok: {
      en: "Yes",
      es: "Sí"
    },
    close: {
      en: "Cancel",
      es: "Cancelar"
    }
  },
  dialogButton: {
    ok: {
      en: "Ok",
      es: "Ok"
    },
    close: {
      en: "Close",
      es: "Cerrar"
    }
  }
}

export const appLang = {
  savefile: {
    ok: {
      en: "File save",
      es: "Archivo guardado"
    },
    error: {
      en: "Don\'t save file",
      es: "No se guardó el archivo"
    }
  },
  deleteFile: {
    ok: {
      en: "The file has been deleted",
      es: "El archivo ha sido eliminado"
    },
    error: {
      en: "The file was not deleted",
      es: "El archivo no fue borrado"
    }
  },
  titleConfigProjectDialog: {
    en: "Config project",
    es: "Configuar proyecto"
  },
  dialogButton: {
    ok: {
      en: "Ok",
      es: "Ok"
    },
    close: {
      en: "Close",
      es: "Cerrar"
    }
  },
  toastProccess: {
    en: "In process",
    es: "En proceso"
  },
  toastUpdateContentDir: {
    en: "Updated files in editor from directory: ",
    es: "Archivos actualizados en el editor desde directorio: "
  }
}

export const tabvieweditorLang = {
  scrollableTabsDefault: {
    en: "Projects loaded in workspace",
    es: "Proyectos cargados en el área de trabajo"
  }
}

export function getTextFromLang(object: any, lang: LANG): string {
  if (lang == LANG.EN) {
    return object.en;
  } else if (lang == LANG.ES) {
    return object.es;
  } else {
    return "noname";
  }
}
