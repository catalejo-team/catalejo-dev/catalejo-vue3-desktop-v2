import 'primeicons/primeicons.css'
import './style/app.css'
import 'primevue/resources/themes/aura-light-green/theme.css'


import PrimeVue from 'primevue/config';
import ToastService from 'primevue/toastservice';
import DialogService from 'primevue/dialogservice';
// import Tooltip from 'primevue/tooltip';


import { createApp } from 'vue'
import App from './App.vue'

// createApp(App).mount('#app')

const app = createApp(App);

// Adder general components
// app.component('Toast', Toast);

// app.directive('tooltip', Tooltip);
app.use(PrimeVue);
app.use(ToastService);
app.use(DialogService);
app.mount('#app');
