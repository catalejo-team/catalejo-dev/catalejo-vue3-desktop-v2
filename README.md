# catalejo-vue3-desktop-v2

## Publicación en NPM

para poder publicar:
1. Edite el archivo `./package.json` aumentando la versión de desarrollo
2. Es necesario descomentar la línea relacionada a "external" en `./vite.config.ts`:
```bash
external: ['../plugins/plugins.js']
```
3. Mantega el archivo `./plugins/plugins.js` limpio de plugins, por ejemplo:
```bash
let plugins = [];
let defaultPaths = [];

// import * as nodemcu from "./nodemcu/nodemcu.es.js";
// plugins.push(nodemcu);

// import * as micropython_esp32 from "./micropython-esp32/main";
// plugins.push(micropython_esp32);

// Directorios para compartir en la aplicación, relativos o absolutos
defaultPaths.push("work")


console.log("plugins to load");
export { plugins, defaultPaths }
```
5. Ejecutar el comando `make build`  y verificando que la carpeta plugins solo contenga
el archivo `plugins.js` y `plugins.d.ts`
6. Ejecute el comando `make publish` y realice la autenticación de dos pasos.

Observación: No suba cambios de la carpeta plugins ni de "external" de `./vite.config.ts`

## Dependencies

* npm i vue-codemirror6
* npm i codemirror
* npm i @codemirror/theme-one-dark
* npm install primevue
* npm install primeicons
* npm i @johnnycubides/blockly-catalejo
* npm i @codemirror/lang-python
* npm i @codemirror/language
* npm i @codemirror/legacy-modes

## Desarrollo de plugins

Se debe tener en cuenta dos detalles:
1. Debe ajustarse el makefile para la regla dev-plugin
2. Se debe mantener comentada en `./vite.config.ts` la línea:
```
// external: ['../plugins/plugins.js']

```

