# Versión de nodejs para la compilación del proyecto
NODE=v21.7.3
# Comando para el gestor de NVM
NVM=sh $$NVM_BIN

## Prueba y desarrollo de plugin
# Ruta del SRC del plugin a desarrollar
# SRC_PLUG=../nodemcu-plugin-catalejo-editor/src
SRC_PLUG=../micropython-esp32-plugin-catalejo-editor/src
# Nombre del plugin (por ejemplo: nodemcu)
# NAME_PLUG=nodemcu
NAME_PLUG=micropython-esp32
# Ruta del plugin en el SRC (no cambiar)
LN_PLUG=./plugins/$(NAME_PLUG)

h: help
d:dev
b: build

install-vue:
	npm create vue@latest

dependencies:
	npm update
	$(NVM) use $(NODE)

init:
	npm install
	# npm run format

dev:
	npm run dev

build:
	rm -rf $(LN_PLUG)
	npm run build

build-only:
	rm -rf $(LN_PLUG)
	npm run build-only

publish: clean build
	npm publish --access public

dev-plugin:
	rm -rf $(LN_PLUG)
	ln -sr $(SRC_PLUG) $(LN_PLUG)

clean:
	rm -rf dist

help:
	@echo "make dependencies -> instalar dependencias"
	@echo "make d -> iniciar proceso de desarrollo"
	@echo "make b -> construyendo dist verificando errores (tener presente que esto borra el enlace simbólico del plugin que se esté desarrollando)"
	@echo "make dev-plugin -> se agrega al desarrollo un plugin del editor"
