import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { viteStaticCopy } from "vite-plugin-static-copy";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue({
      template: {
        compilerOptions: { // agregar tipos para que en el debug sea reconocido
          isCustomElement: tag =>
            tag === 'xml' || tag === 'field' || tag === 'block' ||
            tag === 'value' || tag === 'statement' || tag === 'mutation' ||
            tag === 'arg' || tag === 'variables' || tag === 'variable'
        }
      }
    }),
    viteStaticCopy({
      targets: [
        {
          src: "node_modules/@johnnycubides/blockly-catalejo/media", // Agregando rutas estaticas al copiar
          dest: "",
        },
        {
          src: "./plugins", // Al no ser transpilados los plugins, deberán ser agregados manualmente en dist
          dest: ""
        },
      ],
    })
  ],
  build: {
    rollupOptions: {
      // En modo de desarrollo la siguiente línea "external" deberá se comentada para poder desarrollar los plugins, en modo build debe estar descomentada
      external: ['../plugins/plugins.js'], // Estos archivos no serán transpilados, observar el uso de .js para que sean cargados por el navegador.
      output: {
        manualChunks(id) {
          if (id.includes('node_modules')) {
            if (id.includes('blockly')) {
              return 'blockly';
            }
            if (id.includes('codemirror')) {
              return 'codemirror';
            }
            return id.toString().split('node_modules/')[1].split('/')[0].toString();
          }
        }
      }
    }
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)), // @ es el alias de src
    }
  }
})
